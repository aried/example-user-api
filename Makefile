build:
	docker stop rgexample-mysql && docker rm rgexample-mysql && docker run -d -p 13306:3306 -v ${PWD}/migrations/:/docker-entrypoint-initdb.d --name rgexample-mysql -e MYSQL_ROOT_PASSWORD=password -d mysql:latest --default-authentication-plugin=mysql_native_password
clean:
	docker stop rgexample-mysql && docker rm rgexample-mysql
