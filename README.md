## Rg's Design Pattern: 
This is an example of our new design pattern based on hexagonal, clean architecture, and domain drive development. The example will using the usecase of user-api which consist of authentication functions such as Login, Register, etc and profile's function used by end user (in profile page) or admin (in CMS) like GetProfile, UpdateProfile, etc.


## HOW TO USE IT
```
// make build. This command is needed to setup databases & pre requirement
make build
go run main.go

Notes/Disclaimer:
- migrations folder isn't meant to be like that, use rg-osc format. We use local docker in this service to make it easier to test.
- init db also please use our usual way to init. I'm using internal pkg for the sake of example.
- contract also please use our usual way to init. I'm putting in this repo for the sake of example.
```

## GUIDELINE

Additional Refference:
https://quip.com/zlZUAvKFQtye/New-Design-Pattern
### Step-by-step writing code
- Preparation:
    - Setup contract and initialize service `rogu be new` 
    - Including setup main.go, dependencies in service.yaml, database, config.go, etc.
- Write Business Logic (under `/core` folder)
    - Define entities (look into guideline `How to Define/separate Entity`)
    - Define module (usecase) interface (look into guideline `How to Define/separate Module`)
    - Write module (usecase) implementation.
    - While write module implementation, usually you need to Create/Update/Add repository interface. Then, do Create/Update/Add the repository interface.
    - I suggest you after finish to write the business logic, do MR and wait for code review
- Write external agencies
    - Write repository implementation
    - Including write client folder if needed (or use code gen if possible)
- Wrap it up together
    - Implement usecase to handler
    - Register handler and initialize things in main.go

### How to Define/separate Entity
- Entities encapsulate Enterprise wide business rules. An entity can be an object with methods, or it can be a set of data structures and functions.
- Contract framework (*.proto) is representing our business context, therefore we can look up into *.proto file to get a clue to define the entities
- Not always, but mostly we can look up into rpc's response. Example:
    - `rpc Login(LoginReq) returns (UserToken)` then, maybe UserToken is an entity
    - `rpc GetProfile(GetProfileReq) returns (UserProfile)` then, maybe UserProfile is an entity

### How to Define/separate Repository
- My suggestion is to define the repository based on domain/aggregate root https://docs.microsoft.com/en-us/dotnet/architecture/microservices/microservice-ddd-cqrs-patterns/infrastructure-persistence-layer-design
- You can look into list of entities, then group it / aggregate it (later we're gonna name it aggregate root). 1 Aggregate root = 1 repository.
- Example: `user`, `user_contact`, `user_parent` is under aggregate root `user`. Then we can make it into `UserRepository`
- Repository usually depend to external agencies (database, other-api service, etc.) convert the datas into the format most convenient which understood by the microservice. Therefore, repository usually had a dependencies to `client/*`, `gorm.Db`, `redis`.

```
type user_repository struct{
    db          *gorm.DB
    redis       redis.Client
    notifCli    rg-notification-api.Client
}
func (r *user_repository) Get(id int) entity.User { ... } // implement
func (r *user_repository) GetWithCache(id int) entity.User { ... } // implement
```

### How to Define/separate Module
- In most cases (best practice), module is a usecase layer. Can be other than usecase (domain) but on certain cases.
- Usecase means it strongly tied to user story, so the point of view is user centric. To get clue about defining this, look up into PRD or Usecase Diagram
- In this example, the apis serving 2 usecase / user journey which is authentication usecase and profile usecase. From user perspective, authentication usecase and profile usecase will have it's own user journey. Authentication usecasee should be on login/register page. And profile usecase usually in profile page or CMS page (used by admin).

## Concept
### Principle
- Separation of concern
- Independent of framework
- Independent of UI
- Independent of DB + External agency
- Testable
- Readable 

### Dependency Rule
<img src="https://gitlab.com/ruangguru/source/-/raw/example-user-api/example-new-design-pattern/example-user-api/diagram.png" height="600">

### Folder Structure
    .
    ├── client
    │   ├── api                         # client for rest api dependency
    │       ├── googleapis   
    │       ├── user-token-api  
    │           ├── dto.go   
    │           ├── client.go              
    │   ├── grpc
    │       ├── rg-notification         # generated from dependency (contract framework)
    ├── config                          # service envar config
    │   ├── config.go    
    ├── core                            # core logic will be in this folder
    │   ├── entity                      # encapsulate enterprise wide rule
    │       ├── user.go
    │       ├── usertoken.go
    │       ├── entity1.go
    │       ├── entity2.go
    │   ├── module                      # usecase/service layer
    │       ├── auth-usecase            # usecase layer to handle user journey in authentication
    │       ├── profile-usecase         # usecase layer to handle user journey in profile page / CMS
    │       ├── module-service          # best practice using usecase, but we can use service if it's a simple service 
    │   ├── repository                  # interface adaptors for usecase to communicate with outermost communication
    │       ├── user-repo               # interface for user domain repository
    │       ├── token-repo               
    ├── handler                         # An input port (driving port) lets the application core to expose the functionality to the outside of the world. Handler will communicate to module (usecase layer) to serve the purpose
    │   ├── api
    │       ├── grpc               
    │           ├── user                # code generated (contract framework: *pb.go, *.pb.morse.go, etc.)
    │       ├── handler.go              # Implement the grpc/rest api handler
    │   ├── subscriber
    │       ├── handler.go              # Implement the subscriber handler
    │   ├── worker
    │       ├── handler.go              # Implement the worker handler
    ├── migrations                      # rg-osc migrations file will be here
    │   ├── ...
    ├── pkg                             # Supporting library / script will be written here
    │   ├── helpers                     
    │   ├── password                    
    ├── go.mod
    ├── go.sum
    ├── main.go
    ├── service.yaml
    └── README.md

### FAQ

<hr>

**Q:** Can we grouped a list of entities and create the folder to contain it?
<br>**A:** No problem
<hr>

**Q:** If we need to do transaction query `transaction.Begin()` which contains 2 table, how to do it?
<br>**A:** in repository implementation.

```
type user_repository struct{
    db          *gorm.DB
}
func (r *user_repository) Create(user entity.User) error {
    tx := r.db.Begin()
    
    userDto := UserDto{}.FromUserEntity(user)
    userContactsDto := []UserContactDto{}
    for _, c := range user.Contacts{
        userContactsDto = append(userContactsDto, UserContactDto{}.FromContactEntity(c))
    }

    err := tx.Create(&userDto).Error
    if err != nil {
        tx.Rollback()
        return err
    }

    err = tx.Create(&userContactsDto).Error
    if err != nil {
        tx.Rollback()
        return err
    }
    return tx.Commit()
}

```
<hr>

**Q:** Why the name is module not usecase?
<br>**A:** We are not sure if the module itself should always usecase or not. While the best practice it would be usecase, but let's say in simplle CRUD service, it more suitable to use domain for module.


<hr>

**Q:** Where to put embed file which already supported in Go 1.16?
<br>**A:** Currently we haven't had the standart. Feel free to discuss if you had the cases.

<hr>

**Q:** Why don't we use internal folder instead of core?
<br>**A:** We already consider about it, but we realize that entity folder is needed by repository implementation therefore and this is microservice also, not share library/platform therefore using folder `core` is a good choice.

<hr>

**Q:** Why put repository implementation outside `core` folder?
<br>**A:** Much like `handler`, repository implementation is an outermost layer also which had a purpose communicate with external being (DB, client, etc.). It's quite different with module implementation which is the usecase layer (inner layer).


