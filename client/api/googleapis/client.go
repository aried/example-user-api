package googleapis

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Client struct{}

func NewClient() *Client {
	return &Client{}
}

func (c *Client) GetUserInfo(accessToken string) (*GoogleUserInfo, error) {
	/* mocking the result */
	return &GoogleUserInfo{
		ID:         "1001001",
		Email:      "herry@ruangguru.com",
		Name:       "herry gunawan",
		GivenName:  "herry",
		FamilyName: "gunawan",
		Gender:     "m",
		Picture:    "https://lh3.googleusercontent.com/fife/ABSRlIok0F8FW7OfQzUUMAfzLG73nEWoTlA0JxQ6292tmwGC6YbhpcwXVfczpirZwpqPOoRe7lwqcLJjwldJUIB9od7e0xvjFM0tG4CpGn16_28NYDFh1cN8QsJWSrdKkep359a3OQ0SpNGNMCMs7WleaXKE9QwYK2ZGNUlTNvyFl2waJADmhJSAkyQQBWz5yGzfuhnHzRyZrMT74Yj44AG3n8oVnnoVfMRG2O8wTEZWKJsbBr_RaoI3oWGMsjA93f1oa9tIBxMxNCnJnZCdY50BbAJehIbt5bLuLQrqmYu81shx5L6_5uhMlHZwkcB23LRXB_uWlj5ROZbh2iu9mYO9LL435W3iPKth2we4rSpSHvqOgSlQQFsKbdOly6OCknRGs4s6IYeMITD5-SblpB8wybmrOwY8b67kHHTCphDQbTjD4RgTwtayoucTemKjsw_-_27GKeNmUVGHpwFkbF3ZiXF2keqbK-vZwekqS83R2kFNFjZPhCXVgCgJtMNlNWEXWLZ2cI6HYn_ZgwlFE2hFydhmLPvkEXJ5Avvmr9x-A9Z7rlVFbekipXkSArZtbMiEi1TEibniDMsYZnB8MXghProDXA65Oww--kIs6ma4RqdoJ8gSwDbT6Tt0WJCzESrzlQgdL9lk6Yx9C__4tkYE2Qzas1gHvOLaDFqW7hhdu9ZlE7438D77BUgpAyZQTt6tgd3yuAXXg-tMJF1aPZ5VQT8F0QAtPgTyPUHZAAQsyzgHuA=s64-c",
	}, nil
	url := fmt.Sprintf("https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=%s", accessToken)
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var result GoogleUserInfo
	err = json.Unmarshal(body, &result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}
