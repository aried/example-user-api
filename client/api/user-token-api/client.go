package usertokenapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

const (
	timeout = 10
)

type Client struct {
	host string
}

func NewClient(host string) *Client {
	return &Client{host: host}
}

// GenerateToken(email string) (TokenResponse, error)

func (c *Client) GenerateToken(email string) (*TokenResponse, error) {
	/* mocking the result */
	return &TokenResponse{
		Code: 200,
		Data: TokenResponseData{
			RefreshToken: "refresh_token_mock_result",
			Token:        "access_token_mock_result",
		},
		Status: "success",
	}, nil

	targetURL := fmt.Sprintf("%v/api/v3/generate-token", c.host)

	bodyContent, err := json.Marshal(TokenRequest{Email: email})
	if err != nil {
		return nil, err
	}

	resp, err := http.Post(targetURL, "application/json", strings.NewReader(string(bodyContent)))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	bodyResult, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var result TokenResponse
	err = json.Unmarshal(bodyResult, &result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}
