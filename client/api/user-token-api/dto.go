package usertokenapi

type TokenResponse struct {
	Code   int               `json:"code"`
	Data   TokenResponseData `json:"data"`
	Status string            `json:"status"`
}

type TokenResponseData struct {
	RefreshToken string `json:"refresh_token"`
	Token        string `json:"token"`
}

type TokenRequest struct {
	Email string `json:"email"`
}
