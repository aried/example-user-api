package config

import (
	"github.com/kelseyhightower/envconfig"
)

// Config struct to implement model of inbox configuration
type Config struct {
	GRPCPort                   string `envconfig:"GRPC_PORT" default:"19091"`
	RESTPort                   string `envconfig:"REST_PORT" default:"18081"`
	MorseGracefulTimeout       int    `envconfig:"MORSE_GRACEFUL_TIMEOUT" default:"1"`
	InternalAPIRequestPassword string `envconfig:"INTERNAL_API_REQUEST_PASSWORD"`
	UserValidateTokenHTTPURL   string `envconfig:"AUTH_URL" default:""`

	PasswordSalt  string `envconfig:"PASSWORD_SALT" default:"salt123"`
	SuperPassword string `envconfig:"SUPER_PASSWORD" default:"superpassword123"`
}

// Get to get defined configuration
func Get() Config {
	cfg := Config{}
	envconfig.MustProcess("", &cfg)
	return cfg
}
