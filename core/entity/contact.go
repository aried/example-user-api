package entity

type Contact struct {
	Id        int    `json:"id"`
	Type      string `json:"type"`
	ContactNo string `json:"contact_no"`
}
