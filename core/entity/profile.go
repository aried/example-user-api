package entity

type Profile struct {
	Email    string    `json:"email"`
	Name     string    `json:"name"`
	Age      int       `json:"age"`
	Contacts []Contact `json:"contacts"`
}

func (p *Profile) AddContact(c Contact) {
	if p == nil {
		return
	} else if p.Contacts == nil {
		p.Contacts = []Contact{}
	}

	p.Contacts = append(p.Contacts, c)
}
