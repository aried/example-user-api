package entity

import (
	"time"

	"gitlab.com/ruangguru/source/example-user-api/pkg/helpers"
)

type User struct {
	Id       int
	Email    string
	Password string
	Name     string
	Birthday time.Time
}

func (User) New(email, password, name string, birthday time.Time) *User {
	return &User{
		Email:    email,
		Password: password,
		Name:     name,
		Birthday: birthday,
	}
}

func (u *User) ToProfile() *Profile {
	if u == nil {
		return nil
	}
	return &Profile{
		Email:    u.Email,
		Name:     u.Name,
		Age:      helpers.CountAge(u.Birthday),
		Contacts: []Contact{},
	}
}

func (u *User) SetRandomPasword() {
	if u == nil {
		return
	}

	u.Password = helpers.RandString(10)
}
