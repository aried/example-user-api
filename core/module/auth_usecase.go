package module

import (
	"errors"
	"fmt"
	"time"

	"gitlab.com/ruangguru/source/example-user-api/core/entity"
	"gitlab.com/ruangguru/source/example-user-api/core/repository"
	"gitlab.com/ruangguru/source/example-user-api/pkg/password"
)

type AuthUsecase interface {
	Login(email, password string) (*entity.UserToken, error)
	LoginByGoogle(googleAccessToken string) (*entity.UserToken, error)
	Register(email, password, name string, birthday time.Time) error
}

type authUsecase struct {
	user_repo     repository.UserRepository
	token_repo    repository.TokenRepository
	passwordSvc   password.Service
	superPassword string
}

func NewAuthUsecase(user_repo repository.UserRepository, token_repo repository.TokenRepository, passwordSvc password.Service, superPassword string) AuthUsecase {
	return &authUsecase{user_repo: user_repo, token_repo: token_repo, passwordSvc: passwordSvc, superPassword: superPassword}
}

var ErrEmailNotRegistered = errors.New("email wasn't registered")
var ErrInvalidPassword = errors.New("wrong password")

func (uc *authUsecase) Login(email, password string) (*entity.UserToken, error) {
	userAuth, err := uc.user_repo.GetByEmail(email)
	if err != nil {
		if errors.Is(err, repository.ErrUserNotFound) {
			return nil, fmt.Errorf("%w. got: %s", ErrEmailNotRegistered, email)
		}
		return nil, fmt.Errorf("%w: %v", ErrUnexpected, err)
	}
	if password != uc.superPassword {
		if !uc.passwordSvc.Check(password, userAuth.Password) {
			return nil, ErrInvalidPassword
		}
	}

	/* Generate Token */
	userToken, err := uc.token_repo.Generate(userAuth.Email)
	if err != nil {
		return nil, fmt.Errorf("%w: %v", ErrUnexpected, err)
	}
	return userToken, err
}

func (uc *authUsecase) LoginByGoogle(googleAccessToken string) (*entity.UserToken, error) {
	u, err := uc.user_repo.GetFromGoogleApis(googleAccessToken)
	if err != nil {
		return nil, fmt.Errorf("%w: %v", ErrUnexpected, err)
	}

	needRegister := false
	_, err = uc.user_repo.GetByEmail(u.Email)
	if err != nil {
		if errors.Is(err, repository.ErrUserNotFound) {
			needRegister = true
		} else {
			return nil, fmt.Errorf("%w: %v", ErrUnexpected, err)
		}
	}
	if needRegister {
		err = uc.Register(u.Email, "", u.Name, u.Birthday)
		if err != nil {
			return nil, err
		}
	}

	/* Generate Token */
	userToken, err := uc.token_repo.Generate(u.Email)
	if err != nil {
		return nil, fmt.Errorf("%w: %v", ErrUnexpected, err)
	}
	return userToken, err
}

func (uc *authUsecase) Register(email, password, name string, birthday time.Time) error {
	param := entity.User{}.New(email, password, name, birthday)
	if param.Password == "" {
		param.SetRandomPasword()
	}

	var err error
	param.Password, err = uc.passwordSvc.Generate(param.Password)
	if err != nil {
		return fmt.Errorf("%w: %v", ErrUnexpected, err)
	}

	_, err = uc.user_repo.Create(param)
	if err != nil {
		return fmt.Errorf("%w: %v", ErrUnexpected, err)
	}
	return nil
}
