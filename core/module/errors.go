package module

import "errors"

// SharedErr
var ErrUnexpected = errors.New("unexpected error")
