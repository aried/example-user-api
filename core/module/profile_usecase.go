package module

import (
	"errors"
	"fmt"

	"gitlab.com/ruangguru/source/example-user-api/core/entity"
	"gitlab.com/ruangguru/source/example-user-api/core/repository"
)

type ProfileUsecase interface {
	GetProfile(id int) (*entity.Profile, error)
	AddContact(userId int, contactType, contactNo string) error
	DeleteContact(userId int, contactId int) error
}

type profileUsecase struct {
	user_repo repository.UserRepository
}

func NewProfileUsecase(user_repo repository.UserRepository) ProfileUsecase {
	return &profileUsecase{user_repo: user_repo}
}

var ErrProfileNotFound = errors.New("profile is not found")

func (uc *profileUsecase) GetProfile(id int) (*entity.Profile, error) {
	data, err := uc.user_repo.GetAsProfile(id)
	if err != nil {
		if errors.Is(err, repository.ErrUserNotFound) {
			return nil, fmt.Errorf("%w. id: %d", ErrProfileNotFound, id)
		}
		return nil, fmt.Errorf("%w: %v", ErrUnexpected, err)
	}
	return data, nil
}

func (uc *profileUsecase) AddContact(userId int, contactType, contactNo string) error {
	err := uc.user_repo.AddContact(userId, contactType, contactNo)
	if err != nil {
		return fmt.Errorf("%w: %v", ErrUnexpected, err)
	}
	return nil
}

var ErrUnauthorizedDeleteContact = errors.New("Deleting this contact is prohibited for you")

func (uc *profileUsecase) DeleteContact(userId int, contactId int) error {
	err := uc.user_repo.DeleteContact(userId, contactId)
	if err != nil {
		if errors.Is(err, repository.ErrUserUnauthorizedAction) {
			return ErrUnauthorizedDeleteContact
		}
		return fmt.Errorf("%w: %v", ErrUnexpected, err)
	}
	return nil
}
