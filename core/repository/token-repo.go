package repository

import (
	"errors"

	"gitlab.com/ruangguru/source/example-user-api/core/entity"
)

var ErrGenerateJwtToken = errors.New("failed to generate jwt token")

type TokenRepository interface {
	Generate(email string) (*entity.UserToken, error)
}
