package repository

import (
	"errors"

	"gitlab.com/ruangguru/source/example-user-api/core/entity"
)

var ErrUserNotFound = errors.New("user not found")
var ErrUserUnauthorizedAction = errors.New("unauthorized action")

// Domain User & Profile
type UserRepository interface {
	GetById(id int) (*entity.User, error)
	GetByEmail(email string) (*entity.User, error)
	GetFromGoogleApis(googleToken string) (*entity.User, error)
	AddContact(userId int, contactType, contactNo string) error
	DeleteContact(userId int, contactId int) error
	Create(param *entity.User) (*entity.User, error)
	GetAsProfile(id int) (*entity.Profile, error)

	SendEmail(user *entity.User, title, message string) error
}
