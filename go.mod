module gitlab.com/ruangguru/source/example-user-api

go 1.14

replace gitlab.com/ruangguru/source => ../

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.8.1 // indirect
	github.com/golang/protobuf v1.5.0
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/grpc-ecosystem/grpc-gateway v1.12.2
	github.com/jinzhu/gorm v1.9.16
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/ruang-guru/rg-genproto v1.0.30
	// gitlab.com/ruangguru/source/shared-lib/go v1.0.100 // indirect
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
	google.golang.org/genproto v0.0.0-20191115221424-83cc0476cb11
	google.golang.org/grpc v1.27.1
	gopkg.in/validator.v2 v2.0.0-20200605151824-2b28d334fa05
	gorm.io/driver/mysql v1.0.5
	gorm.io/gorm v1.21.8
)
