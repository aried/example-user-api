package userapi

import (
	"gitlab.com/ruangguru/source/example-user-api/core/entity"
)

func (UserProfile) FromProfileEntity(in entity.Profile) *UserProfile {
	out := &UserProfile{
		Email:    in.Email,
		Name:     in.Name,
		Age:      int32(in.Age),
		Contacts: []*UserContact{},
	}

	for _, c := range in.Contacts {
		out.Contacts = append(out.Contacts, UserContact{}.FromContactEntity(c))
	}

	return out
}

func (UserContact) FromContactEntity(in entity.Contact) *UserContact {
	out := &UserContact{
		Id:        int32(in.Id),
		Type:      in.Type,
		ContactNo: in.ContactNo,
	}

	return out
}
