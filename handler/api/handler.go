package apihandler

import (
	"context"
	"errors"
	"time"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/ruangguru/source/example-user-api/core/module"
	"gitlab.com/ruangguru/source/example-user-api/core/repository"
	pbuserapi "gitlab.com/ruangguru/source/example-user-api/handler/api/grpc/user"
)

type handler struct {
	profile_uc module.ProfileUsecase
	auth_uc    module.AuthUsecase
}

func New(profile_uc module.ProfileUsecase, auth_uc module.AuthUsecase) pbuserapi.MorseUserapiServer {
	return &handler{profile_uc: profile_uc, auth_uc: auth_uc}
}

func (h *handler) Login(ctx context.Context, req *pbuserapi.LoginReq, clientContext *pbuserapi.ClientContext) (*pbuserapi.UserToken, *pbuserapi.LoginError) {
	data, err := h.auth_uc.Login(req.Email, req.Password)
	if err != nil {
		if errors.Is(err, module.ErrEmailNotRegistered) {
			return nil, pbuserapi.LoginError{}.FromDefaultError(pbuserapi.DEFAULT_ERROR_BAD_REQUEST).WithMessage(err.Error()).WithOtherErrors([]pbuserapi.OtherError{pbuserapi.OtherError{Code: 1001, Field: "email", Message: "email isn't registered"}})
		} else if errors.Is(err, module.ErrInvalidPassword) {
			return nil, pbuserapi.LoginError{}.FromDefaultError(pbuserapi.DEFAULT_ERROR_BAD_REQUEST).WithMessage(err.Error()).WithOtherErrors([]pbuserapi.OtherError{pbuserapi.OtherError{Code: 1002, Field: "password", Message: "wrong password"}})
		}
		return nil, pbuserapi.LoginError{}.FromDefaultError(pbuserapi.DEFAULT_ERROR_INTERNAL_SERVER).WithMessage(err.Error())
	}
	return &pbuserapi.UserToken{AccessToken: data.AccessToken, RefreshToken: data.RefreshToken}, nil
}

func (h *handler) LoginGoogle(ctx context.Context, req *pbuserapi.LoginGoogleReq, clientContext *pbuserapi.ClientContext) (*pbuserapi.UserToken, *pbuserapi.LoginGoogleError) {
	data, err := h.auth_uc.LoginByGoogle(req.GoogleToken)
	if err != nil {
		return nil, pbuserapi.LoginGoogleError{}.FromDefaultError(pbuserapi.DEFAULT_ERROR_INTERNAL_SERVER).WithMessage(err.Error())
	}
	return &pbuserapi.UserToken{AccessToken: data.AccessToken, RefreshToken: data.RefreshToken}, nil
}

func (h *handler) Register(ctx context.Context, req *pbuserapi.RegisterReq, clientContext *pbuserapi.ClientContext) (*empty.Empty, *pbuserapi.RegisterError) {
	birthday, err := time.Parse(time.RFC3339, req.Birthday)
	if err != nil {
		return nil, pbuserapi.RegisterError{}.FromDefaultError(pbuserapi.DEFAULT_ERROR_BAD_REQUEST).WithMessage(err.Error()) //.WithOtherError()
	}

	err = h.auth_uc.Register(req.Email, req.Password, req.Name, birthday)
	if err != nil {
		return nil, pbuserapi.RegisterError{}.FromDefaultError(pbuserapi.DEFAULT_ERROR_INTERNAL_SERVER).WithMessage(err.Error())
	}
	return &empty.Empty{}, nil
}

func (h *handler) GetProfile(ctx context.Context, req *pbuserapi.GetProfileReq, clientContext *pbuserapi.ClientContext) (*pbuserapi.UserProfile, *pbuserapi.GetProfileError) {
	data, err := h.profile_uc.GetProfile(int(req.Id))
	if err != nil {
		if errors.Is(err, module.ErrProfileNotFound) {
			return nil, pbuserapi.GetProfileError__PROFILE_NOT_FOUND.WithMessageInternal(err.Error())
		}
		return nil, pbuserapi.GetProfileError{}.FromDefaultError(pbuserapi.DEFAULT_ERROR_INTERNAL_SERVER).WithMessage(err.Error())
	}

	return (&pbuserapi.UserProfile{}).FromProfileEntity(*data), nil
}

func (h *handler) AddContact(ctx context.Context, req *pbuserapi.AddContactReq, clientContext *pbuserapi.ClientContext) (*empty.Empty, *pbuserapi.AddContactError) {
	err := h.profile_uc.AddContact(int(req.UserId), req.Type, req.ContactNo)
	if err != nil {
		return nil, pbuserapi.AddContactError{}.FromDefaultError(pbuserapi.DEFAULT_ERROR_INTERNAL_SERVER).WithMessage(err.Error())
	}
	return &empty.Empty{}, nil
}

func (h *handler) DeleteContact(ctx context.Context, req *pbuserapi.DeleteContactReq, clientContext *pbuserapi.ClientContext) (*empty.Empty, *pbuserapi.DeleteContactError) {
	err := h.profile_uc.DeleteContact(int(req.UserId), int(req.ContactId))
	if err != nil {
		if errors.Is(err, repository.ErrUserUnauthorizedAction) {

		}
		return nil, pbuserapi.DeleteContactError{}.FromDefaultError(pbuserapi.DEFAULT_ERROR_INTERNAL_SERVER).WithMessage(err.Error())
	}
	return &empty.Empty{}, nil
}
