package main

import (
	"gitlab.com/ruangguru/source/example-user-api/client/api/googleapis"
	pbNotification "gitlab.com/ruangguru/source/example-user-api/client/grpc/rg-notification/notification"
	"gitlab.com/ruangguru/source/example-user-api/config"
	"gitlab.com/ruangguru/source/example-user-api/core/module"
	apihandler "gitlab.com/ruangguru/source/example-user-api/handler/api"
	pbuserapi "gitlab.com/ruangguru/source/example-user-api/handler/api/grpc/user"
	"gitlab.com/ruangguru/source/example-user-api/pkg/conn"
	"gitlab.com/ruangguru/source/example-user-api/pkg/password"
	"gitlab.com/ruangguru/source/example-user-api/repository/token_repository"
	"gitlab.com/ruangguru/source/example-user-api/repository/user_repository"
	authInt "gitlab.com/ruangguru/source/shared-lib/go/contract/middleware/auth"
	logInt "gitlab.com/ruangguru/source/shared-lib/go/contract/middleware/logger"
	recInt "gitlab.com/ruangguru/source/shared-lib/go/middleware/grpc/recovery"
	"gitlab.com/ruangguru/source/shared-lib/go/morse"
	"gitlab.com/ruangguru/source/shared-lib/go/morse/client"
	"gitlab.com/ruangguru/source/shared-lib/go/morse/graceful"
	"gorm.io/gorm/logger"
)

func main() {
	cfg := config.Get()

	// We are using docker for mysql dependencies, Actually we're not gonna use this in production
	db := conn.NewMysqlCli("localhost", "13306", "root", "password", "rgexample", logger.Info).Connect()

	user_repo := user_repository.New(db, googleapis.NewClient(), pbNotification.NewNotificationCli(client.ServerAddr("localhost:19092")))
	token_repo := token_repository.New("secret1", 3600, "secret2", 7200)
	// token_repo := token_repository_v2.New(usertokenapi.NewClient("localhost:18083"))

	profile_uc := module.NewProfileUsecase(user_repo)
	auth_uc := module.NewAuthUsecase(user_repo, token_repo, password.NewBcryptPassword(cfg.PasswordSalt), cfg.SuperPassword)

	hndl := apihandler.New(profile_uc, auth_uc)

	svc := morse.NewService(
		morse.GRPCPort(cfg.GRPCPort),
		morse.RESTPort(cfg.RESTPort),
		morse.EnablePrometheus(),
	)

	// Initialize morse interceptor
	authenticationInt := authInt.UnaryServerInterceptor(
		cfg.InternalAPIRequestPassword,
		"localhost:29000",
		pbuserapi.Permissions(),
	)

	loggerInt := logInt.UnaryServerInterceptor()
	recoverInt := recInt.UnaryServerInterceptor()
	svc.UseServerUnaryInterceptor(loggerInt, recoverInt, authenticationInt)

	// Initialize morse
	svc.Init()

	// Register handlers
	pbuserapi.RegisterUserapi(svc, hndl)

	// Run
	graceful.RunMorseGracefully(cfg.MorseGracefulTimeout, svc)
}
