-- in this example dont use rg-osc since we will gonna use docker for testing purpose
USE rgexample;

INSERT INTO example_user (email, password, name, birthday) values ('herry@ruangguru.com', '$2a$10$6qG53332t/jm.KRQ5XXidO/ouPPGvYGbahYM6DcY.pXETIKgYpBvi', 'Herry Gunawan', '2000-01-01');
INSERT INTO example_user (email, password, name, birthday) values ('alvin@ruangguru.com', '$2a$10$rf7PtLvoHb7TRFlGIfhuCOyWykcpUql9ABJzZI6ceSTDwjbWK39N.', 'Alvin Francis', '1980-03-10');
INSERT INTO example_user (email, password, name, birthday) values ('iqbal@ruangguru.com', '$2a$10$BRMnQwIccZmsC61PqUbiLumn0uyZuIZ7wxzvYdj/4RTO2p1ZGLhSi', 'Iqbal Nur Hakim', '1985-06-20');

INSERT INTO example_user_contact (user_id, type, contact_no) values (1, 'whatsapp', '0819192342');
INSERT INTO example_user_contact (user_id, type, contact_no) values (1, 'telephone', '0215512312');
INSERT INTO example_user_contact (user_id, type, contact_no) values (1, 'phone', '0819192342');
INSERT INTO example_user_contact (user_id, type, contact_no) values (1, 'email', 'hg@gmail.com');
INSERT INTO example_user_contact (user_id, type, contact_no) values (2, 'whatsapp', '0822123177');
INSERT INTO example_user_contact (user_id, type, contact_no) values (3, 'whatsapp', '0861949124');
INSERT INTO example_user_contact (user_id, type, contact_no) values (3, 'email', 'iqbal@gmail.com');
