package helpers

import (
	"math/rand"
	"time"
)

const randStringBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func RandString(n int) string {
	rand.Seed(time.Now().UnixNano())
	b := make([]byte, n)
	for i := range b {
		b[i] = randStringBytes[rand.Intn(len(randStringBytes))]
	}
	return string(b)
}
