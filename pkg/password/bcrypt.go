package password

import "golang.org/x/crypto/bcrypt"

type bcryptPassword struct {
	salt string
}

func NewBcryptPassword(salt string) Service {
	return &bcryptPassword{salt: salt}
}

func (s *bcryptPassword) Generate(password string) (string, error) {
	out, err := bcrypt.GenerateFromPassword([]byte(s.withSalt(password)), bcrypt.DefaultCost)
	return string(out), err
}

func (s *bcryptPassword) Check(password, hashedPassword string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(s.withSalt(password)))
	return err == nil
}

func (s *bcryptPassword) withSalt(password string) string {
	return password + "|" + s.salt
}
