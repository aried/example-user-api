package password

type Service interface {
	Generate(password string) (string, error)
	Check(password, hashedPassword string) bool
}
