package token_repository

import (
	"fmt"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/ruangguru/source/example-user-api/core/entity"
	repository_intf "gitlab.com/ruangguru/source/example-user-api/core/repository"
)

/* direct token implementation */

type repository struct {
	accessTokenSecret  string
	accessTokenExpiry  int
	refreshTokenSecret string
	refreshTokenExpiry int
}

func New(accessTokenSecret string, accessTokenExpiry int, refreshTokenSecret string, refreshTokenExpiry int) repository_intf.TokenRepository {
	return &repository{
		accessTokenSecret:  accessTokenSecret,
		accessTokenExpiry:  accessTokenExpiry,
		refreshTokenSecret: refreshTokenSecret,
		refreshTokenExpiry: refreshTokenExpiry,
	}
}

func (r *repository) Generate(email string) (*entity.UserToken, error) {
	/* logic of usertoken */
	jwtClaims := RefreshTokenClaim{}.New(email, r.refreshTokenExpiry).ToJwtClaims()
	refreshToken, err := generateToken(jwtClaims, r.refreshTokenSecret)
	if err != nil {
		return nil, err
	}

	jwtClaims = AccessTokenClaim{}.New(email, refreshToken, r.accessTokenExpiry).ToJwtClaims()
	accessToken, err := generateToken(jwtClaims, r.accessTokenSecret)
	if err != nil {
		return nil, err
	}

	return &entity.UserToken{AccessToken: accessToken, RefreshToken: refreshToken}, nil
}

func generateToken(claims jwt.MapClaims, secretKey string) (token string, err error) {
	jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token, err = jwtToken.SignedString([]byte(secretKey))
	if err != nil {
		err = fmt.Errorf("<%w>: %s", repository_intf.ErrGenerateJwtToken, err.Error())
	}
	return
}
