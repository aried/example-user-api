package token_repository_v2

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

type AccessTokenClaim struct {
	Email        string
	RefreshToken string
	issuedTime   time.Time
	expiredTime  time.Time
}

func (AccessTokenClaim) New(email string, refreshToken string, expiryInSecond int) AccessTokenClaim {
	return AccessTokenClaim{
		Email:        email,
		RefreshToken: refreshToken,
		issuedTime:   time.Now().UTC(),
		expiredTime:  time.Now().Add(time.Second * time.Duration(expiryInSecond)).UTC()}
}
func (atc AccessTokenClaim) ToJwtClaims() jwt.MapClaims {
	return jwt.MapClaims{
		"email":         atc.Email,
		"refresh_token": atc.RefreshToken,
		"issued_time":   atc.issuedTime.Unix(),
		"expired_time":  atc.expiredTime.Unix(),
	}
}

type RefreshTokenClaim struct {
	Email       string
	issuedTime  time.Time
	expiredTime time.Time
}

func (RefreshTokenClaim) New(email string, expiryInSecond int) RefreshTokenClaim {
	return RefreshTokenClaim{
		Email:       email,
		issuedTime:  time.Now().UTC(),
		expiredTime: time.Now().Add(time.Second * time.Duration(expiryInSecond)).UTC()}
}

func (rtc RefreshTokenClaim) ToJwtClaims() jwt.MapClaims {
	return jwt.MapClaims{
		"email":        rtc.Email,
		"issued_time":  rtc.issuedTime.Unix(),
		"expired_time": rtc.expiredTime.Unix(),
	}
}
