package token_repository_v2

import (
	"fmt"

	"github.com/dgrijalva/jwt-go"
	userToken "gitlab.com/ruangguru/source/example-user-api/client/api/user-token-api"
	"gitlab.com/ruangguru/source/example-user-api/core/entity"
	repository_intf "gitlab.com/ruangguru/source/example-user-api/core/repository"
)

/* calling another service implementation */

type repository struct {
	userTokenCli userToken.Client
}

func New(userTokenCli userToken.Client) repository_intf.TokenRepository {
	return &repository{userTokenCli: userTokenCli}
}

func (r *repository) Generate(email string) (*entity.UserToken, error) {
	/* logic of usertoken */
	tokenResp, err := r.userTokenCli.GenerateToken(email)
	if err != nil {
		return nil, err
	}

	return &entity.UserToken{AccessToken: tokenResp.Data.Token, RefreshToken: tokenResp.Data.RefreshToken}, nil
}

func generateToken(claims jwt.MapClaims, secretKey string) (token string, err error) {
	jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token, err = jwtToken.SignedString([]byte(secretKey))
	if err != nil {
		err = fmt.Errorf("<%w>: %s", repository_intf.ErrGenerateJwtToken, err.Error())
	}
	return
}
