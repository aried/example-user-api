package user_repository

import (
	"time"

	"gitlab.com/ruangguru/source/example-user-api/core/entity"
	"gitlab.com/ruangguru/source/example-user-api/pkg/datatype"
	"gitlab.com/ruangguru/source/example-user-api/pkg/helpers"
)

type User struct {
	Id        int           `gorm:"primary_key;column:id"`
	Email     string        `gorm:"column:email"`
	Password  string        `gorm:"column:password"`
	Name      string        `gorm:"column:name"`
	Birthday  datatype.Date `gorm:"column:birthday"`
	CreatedAt *time.Time    `gorm:"column:created_at"`
	UpdatedAt *time.Time    `gorm:"column:updated_at"`
}

func (u *User) ToProfileEntity() *entity.Profile {
	if u == nil {
		return nil
	}

	return &entity.Profile{
		Email:    u.Email,
		Name:     u.Name,
		Age:      helpers.CountAge(u.Birthday.Time()),
		Contacts: []entity.Contact{},
	}
}
func (u *User) ToUserEntity() *entity.User {
	if u == nil {
		return nil
	}

	return &entity.User{
		Id:       u.Id,
		Email:    u.Email,
		Password: u.Password,
		Name:     u.Name,
		Birthday: u.Birthday.Time(),
	}
}
func (User) FromUserEntity(user *entity.User) *User {
	return &User{
		Id:       user.Id,
		Email:    user.Email,
		Password: user.Password,
		Name:     user.Name,
		Birthday: datatype.Date(user.Birthday),
	}
}

type Contact struct {
	Id        int        `gorm:"primary_key;column:id"`
	UserId    int        `gorm:"column:user_id"`
	Type      string     `gorm:"column:type"`
	ContactNo string     `gorm:"column:contact_no"`
	CreatedAt *time.Time `gorm:"column:created_at"`
	UpdatedAt *time.Time `gorm:"column:updated_at"`
}

func (c *Contact) ToContactEntity() *entity.Contact {
	if c == nil {
		return nil
	}
	return &entity.Contact{
		Id:        c.Id,
		Type:      c.Type,
		ContactNo: c.ContactNo,
	}
}
