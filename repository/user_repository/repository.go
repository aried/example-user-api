package user_repository

import (
	"errors"
	"time"

	"gitlab.com/ruangguru/source/example-user-api/client/api/googleapis"
	"gitlab.com/ruangguru/source/example-user-api/client/grpc/rg-notification/notification"
	"gitlab.com/ruangguru/source/example-user-api/core/entity"
	repository_intf "gitlab.com/ruangguru/source/example-user-api/core/repository"
	"gorm.io/gorm"
)

type repository struct {
	db              *gorm.DB
	googleApiCli    *googleapis.Client
	notificationCli notification.NotificationClient
}

func New(db *gorm.DB, googleApiCli *googleapis.Client, notificationCli notification.NotificationClient) repository_intf.UserRepository {
	return &repository{db: db, googleApiCli: googleApiCli, notificationCli: notificationCli}
}

func (r *repository) GetById(id int) (*entity.User, error) {
	userModel := &User{}
	err := r.db.Table("example_user").Where("id = ?", id).First(&userModel).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, repository_intf.ErrUserNotFound
		}
		return nil, err
	}
	return userModel.ToUserEntity(), nil
}
func (r *repository) GetByEmail(email string) (*entity.User, error) {
	userModel := &User{}
	err := r.db.Table("example_user").Where("email = ?", email).First(&userModel).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, repository_intf.ErrUserNotFound
		}
		return nil, err
	}
	return userModel.ToUserEntity(), nil
}

func (r *repository) GetFromGoogleApis(googleToken string) (*entity.User, error) {
	userInfo, err := r.googleApiCli.GetUserInfo(googleToken)
	if err != nil {
		return nil, err
	}
	return &entity.User{
		Email:    userInfo.Email,
		Name:     userInfo.FamilyName + " " + userInfo.GivenName,
		Password: "",
		Birthday: time.Date(1970, time.January, 1, 0, 0, 0, 0, time.UTC),
	}, nil
}

func (r *repository) Create(param *entity.User) (*entity.User, error) {
	userModel := User{}.FromUserEntity(param)

	err := r.db.Table("example_user").Create(&userModel).Error
	if err != nil {
		return nil, err
	}

	return userModel.ToUserEntity(), nil
}

func (r *repository) GetAsProfile(id int) (*entity.Profile, error) {
	user, err := r.GetById(id)
	if err != nil {
		return nil, err
	}

	resp := user.ToProfile()

	contactModels := []*Contact{}
	err = r.db.Table("example_user_contact").Where("user_id = ?", id).Find(&contactModels).Error
	if err != nil {
		return nil, err
	}

	for _, c := range contactModels {
		resp.AddContact(*c.ToContactEntity())
	}
	return resp, nil
}

func (r *repository) AddContact(userId int, contactType, contactNo string) error {
	data := Contact{
		UserId:    userId,
		Type:      contactType,
		ContactNo: contactNo,
	}
	return r.db.Table("example_user_contact").Create(&data).Error
}

func (r *repository) DeleteContact(userId int, contactId int) error {
	data := &Contact{}
	err := r.db.Table("example_user_contact").Where("id = ?", contactId).First(&data).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil
		}
		return nil
	}
	if data.UserId != userId {
		return repository_intf.ErrUserUnauthorizedAction
	}
	return r.db.Table("example_user_contact").Where("id = ?", contactId).Delete(&Contact{}).Error
}

func (r *repository) SendEmail(user *entity.User, title, message string) error {
	/* Send Email logic */

	// _, err := r.notificationCli.NotifyUsers(context.Background(), &notification.NotifyRequest{
	// 	UserIds: []string{user.Id},
	// 	Message: &notification.Message{
	// 		Title: title,
	// 		Content: message.
	// 	},
	// })
	return nil
}
