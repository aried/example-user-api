package user_repository_v2

import (
	"errors"
	"time"

	"gitlab.com/ruangguru/source/example-user-api/client/api/googleapis"
	"gitlab.com/ruangguru/source/example-user-api/client/grpc/rg-notification/notification"
	"gitlab.com/ruangguru/source/example-user-api/core/entity"
	repository_intf "gitlab.com/ruangguru/source/example-user-api/core/repository"
	"gorm.io/gorm"
)

type repository struct {
	db              *gorm.DB
	googleApiCli    *googleapis.Client
	notificationCli notification.NotificationClient
}

func New(db *gorm.DB, googleApiCli *googleapis.Client, notificationCli notification.NotificationClient) repository_intf.UserRepository {
	return &repository{db: db, googleApiCli: googleApiCli, notificationCli: notificationCli}
}

func (r *repository) GetById(id int) (*entity.User, error) {
	userModel := &User{}
	err := r.db.Table("example_user").Where("id = ?", id).First(&userModel).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, repository_intf.ErrUserNotFound
		}
		return nil, err
	}
	return userModel.ToUserEntity(), nil
}

func (r *repository) GetByEmail(email string) (*entity.User, error) {
	userModel := &User{}
	err := r.db.Table("example_user").Where("email = ?", email).First(&userModel).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, repository_intf.ErrUserNotFound
		}
		return nil, err
	}
	return userModel.ToUserEntity(), nil
}

func (r *repository) GetFromGoogleApis(googleToken string) (*entity.User, error) {
	userInfo, err := r.googleApiCli.GetUserInfo(googleToken)
	if err != nil {
		return nil, err
	}
	return &entity.User{
		Email:    userInfo.Email,
		Name:     userInfo.FamilyName + " " + userInfo.GivenName,
		Password: "",
		Birthday: time.Date(1970, time.January, 1, 0, 0, 0, 0, time.UTC),
	}, nil
}

func (r *repository) Create(param *entity.User) (*entity.User, error) {
	userModel := User{}.FromUserEntity(param)

	err := r.db.Table("example_user").Create(&userModel).Error
	if err != nil {
		return nil, err
	}

	return userModel.ToUserEntity(), nil
}

func (r *repository) GetAsProfile(id int) (*entity.Profile, error) {
	datas := []*UserWithContact{}
	err := r.db.Raw(`SELECT u.*, c.type as contact_type, c.contact_no 
		FROM example_user u
		LEFT JOIN example_user_contact c on u.id = c.user_id
		WHERE u.id = ?`, id).Find(&datas).Error
	if err != nil {
		return nil, err
	} else if len(datas) == 0 {
		return nil, repository_intf.ErrUserNotFound
	}

	resp := datas[0].ToProfileEntity()
	for _, data := range datas {
		if data.ContactType == "" {
			continue
		}
		resp.AddContact(entity.Contact{Type: data.ContactType, ContactNo: data.ContactNo})
	}
	return resp, nil
}

func (r *repository) SendEmail(user *entity.User, title, message string) error {
	/* Send Email logic */

	// _, err := r.notificationCli.NotifyUsers(context.Background(), &notification.NotifyRequest{
	// 	UserIds: []string{user.Id},
	// 	Message: &notification.Message{
	// 		Title: title,
	// 		Content: message.
	// 	},
	// })
	return nil
}

func (r *repository) AddContact(userId int, contactType, contactNo string) error {
	data := Contact{
		UserId:    userId,
		Type:      contactType,
		ContactNo: contactNo,
	}
	return r.db.Table("example_user_contact").Create(&data).Error
}

func (r *repository) DeleteContact(userId int, contactId int) error {
	data := &Contact{}
	err := r.db.Table("example_user_contact").Where("id = ?", contactId).Find(&data).Error
	if err != nil {
		return nil
	}
	if data.UserId != userId {
		return repository_intf.ErrUserUnauthorizedAction
	}
	return r.db.Table("example_user_contact").Where("id = ?", contactId).Delete(&Contact{}).Error
}
